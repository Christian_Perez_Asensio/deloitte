FROM openjdk:11-jre-slim

LABEL maintainer = "Christian"

WORKDIR /app

ARG JAR_FILE
ADD target/${JAR_FILE} /app/online-todo-list.jar

EXPOSE 8080
ENTRYPOINT ["java", "-jar", "/app/online-todo-list.jar"]