package com.deloitte.assessment.onlinetodolist.repository;

import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskEntity;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertFalse;

import com.deloitte.assessment.onlinetodolist.model.entity.Task;
import java.util.Optional;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

@DataJpaTest
@AutoConfigureTestDatabase(replace = AutoConfigureTestDatabase.Replace.NONE)
class TaskRepositoryTest {

  @Autowired
  private TaskRepository taskRepository;

  @Test
  void saveTask() {
    Task taskPersisted = save();
    Optional<Task> taskRetrieved = taskRepository.findById(taskPersisted.getId());
    assertThat(taskRetrieved.get()).isEqualTo(taskPersisted);
  }

  @Test
  void findByName() {
    Task taskPersisted = save();
    Optional<Task> taskRetrieved = taskRepository.findByName(taskPersisted.getName());
    assertThat(taskRetrieved.get()).isEqualTo(taskPersisted);
  }

  @Test
  void deleteTask() {
    Task taskPersisted = save();
    Optional<Task> taskRetrieved = taskRepository.findByName(taskPersisted.getName());
    assertThat(taskRetrieved.get()).isEqualTo(taskPersisted);

    taskRepository.deleteById(taskPersisted.getId());
    taskRetrieved = taskRepository.findByName(taskPersisted.getName());
    assertFalse(taskRetrieved.isPresent());
  }

  private Task save() {
    return taskRepository.save(createTaskEntity("Sample task name"));
  }
}