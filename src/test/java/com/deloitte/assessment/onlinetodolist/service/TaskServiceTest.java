package com.deloitte.assessment.onlinetodolist.service;

import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskEntity;
import static java.util.Collections.emptyList;
import static java.util.Collections.nCopies;
import static org.apache.commons.collections4.CollectionUtils.isEmpty;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.deloitte.assessment.onlinetodolist.exception.DuplicatedException;
import com.deloitte.assessment.onlinetodolist.exception.NotFoundException;
import com.deloitte.assessment.onlinetodolist.model.entity.Task;
import com.deloitte.assessment.onlinetodolist.repository.TaskRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
class TaskServiceTest {

  private static final String VALID_NAME = "Sample task name";

  @InjectMocks
  private TaskService taskService;
  @Mock
  private TaskRepository taskRepository;

  @Test
  void addTask() {
    Task expectedTask = createTaskEntity(VALID_NAME);
    when(taskRepository.save(expectedTask)).thenReturn(expectedTask);
    Task actualTask = taskService.addTask(expectedTask);
    assertEquals(expectedTask, actualTask);
  }

  @Test
  void addNullTask() {
    assertThrows(IllegalArgumentException.class, () -> taskService.addTask(null), "Task must not be null");
  }

  @Test
  void addTaskWithExistingName() {
    Task task = createTaskEntity(VALID_NAME);
    when(taskRepository.findByName(eq(VALID_NAME))).thenReturn(Optional.of(task));

    assertThrows(DuplicatedException.class, () -> taskService.addTask(task));
  }

  @Test
  void getAllTasks() {
    List<Task> tasks = nCopies(2, createTaskEntity(VALID_NAME));
    when(taskRepository.findAll()).thenReturn(tasks);

    List<Task> allTasks = taskService.getAllTasks();
    assertEquals(tasks, allTasks);
  }

  @Test
  void getAllTasksNotFound() {
    when(taskRepository.findAll()).thenReturn(emptyList());

    assertTrue(isEmpty(taskService.getAllTasks()));
  }

  @Test
  void deleteTask() {
    UUID id = UUID.randomUUID();
    Task task = createTaskEntity("Sample task name");
    task.setId(id);
    when(taskRepository.findById(id)).thenReturn(Optional.of(task));

    taskService.deleteTask(id);
    verify(taskRepository, times(1)).delete(task);
  }

  @Test
  void deleteTaskIdNotFound() {
    UUID id = UUID.randomUUID();
    when(taskRepository.findById(id)).thenReturn(Optional.empty());

    assertThrows(NotFoundException.class, () -> taskService.deleteTask(id),
        "Could not find a task with the specified id");
  }
}