package com.deloitte.assessment.onlinetodolist.util;

import static java.nio.charset.Charset.defaultCharset;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.io.IOException;
import java.lang.reflect.Constructor;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang3.exception.ExceptionUtils;

public final class TestUtils {

  public static <T> void attemptToCreateInstanceOf(Class<T> clazz) throws Throwable {
    Constructor<T> constructor = clazz.getDeclaredConstructor();
    constructor.setAccessible(true);

    try {
      constructor.newInstance();
    } catch (Exception e) {
      throw ExceptionUtils.getRootCause(e);
    }
  }

  public static String readData(String sourceJsonFile) throws IOException {
    return IOUtils.toString(TestUtils.class.getResourceAsStream(sourceJsonFile), defaultCharset());
  }

  public static byte[] convertObjectToJsonBytes(Object object) throws IOException {
    ObjectMapper mapper = new ObjectMapper();
    mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    return mapper.writeValueAsBytes(object);
  }
}