package com.deloitte.assessment.onlinetodolist.util;

import com.deloitte.assessment.onlinetodolist.model.dto.TaskDto;
import com.deloitte.assessment.onlinetodolist.model.entity.Task;

public class TaskBuilderUtils {

  private TaskBuilderUtils() {
    throw new AssertionError("Suppress default constructor to prevent instantiation");
  }

  public static TaskDto createTaskDto() {
    TaskDto TaskDto = new TaskDto();
    TaskDto.setName("Sample task name");
    return TaskDto;
  }

  public static TaskDto createInvalidTaskDto() {
    return new TaskDto();
  }

  public static Task createTaskEntity(String name) {
    Task task = new Task();
    task.setName(name);
    return task;
  }
}