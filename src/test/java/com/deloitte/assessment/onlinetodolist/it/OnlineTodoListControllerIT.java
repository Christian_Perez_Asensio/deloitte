package com.deloitte.assessment.onlinetodolist.it;

import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskDto;
import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskEntity;
import static com.deloitte.assessment.onlinetodolist.util.TestUtils.convertObjectToJsonBytes;
import static com.deloitte.assessment.onlinetodolist.util.TestUtils.readData;
import static org.hamcrest.Matchers.greaterThan;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.deloitte.assessment.onlinetodolist.OnlineTodoListApi;
import com.deloitte.assessment.onlinetodolist.model.dto.TaskDto;
import com.deloitte.assessment.onlinetodolist.repository.TaskRepository;
import java.io.IOException;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

@ExtendWith(SpringExtension.class)
@SpringBootTest(classes = OnlineTodoListApi.class)
//@AutoConfigureMockMvc//(secure=false)
//@WithMockUser
class OnlineTodoListControllerIT {

  private static final String TASKS_ENDPOINT = "/tasks";
  private static final String TASKS_ENDPOINT_BY_ID = "/tasks/{id}";

  @Autowired
  private TaskRepository taskRepository;
//  @Autowired
  private MockMvc mockMvc;

  @Autowired
  private WebApplicationContext webApplicationContext;

  @AfterEach
  void tearDown() {
    taskRepository.deleteAll();
  }

  @BeforeEach
  void setUp() throws IOException {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();
    taskRepository.saveAll(List.of(createTaskEntity("Sample task name 11"), createTaskEntity("Sample Task Name 12")));
  }

  @Test
  void addTask() throws Exception {
    ResultActions resultActions = mockMvc.perform(post(TASKS_ENDPOINT).contentType(APPLICATION_JSON)
        .content(readData("/requests/valid_task_request.json")))
        .andExpect(status().isCreated());
    checkTask(resultActions);
  }

  @Test
  void addInvalidTask() throws Exception {
    mockMvc.perform(post(TASKS_ENDPOINT).contentType(APPLICATION_JSON)
        .content(readData("/requests/invalid_task_request.json")))
        .andExpect(status().isBadRequest());
  }

  @Test
  void addTaskWithExistingName() throws Exception {
    TaskDto existingTaskDto = createTaskDto();
    existingTaskDto.setName("Sample task name 11");

    mockMvc.perform(post(TASKS_ENDPOINT).contentType(APPLICATION_JSON)
        .content(convertObjectToJsonBytes(existingTaskDto)))
        .andExpect(status().isBadRequest());
  }

  @Test
  void getAllTasks() throws Exception {
    ResultActions resultActions = mockMvc.perform(get(TASKS_ENDPOINT))
        .andExpect(status().isOk());
    checkTasks(resultActions);
  }

  @Test
  void getAllTasksNotFound() throws Exception {
    tearDown();
    mockMvc.perform(get(TASKS_ENDPOINT))
        .andExpect(status().isOk())
        .andExpect(content().string("[]"));
  }

  @Test
  void deleteTask() throws Exception {
    taskRepository.save(createTaskEntity("Sample task name 3"));
    UUID taskId = taskRepository.findAll().iterator().next().getId();

    mockMvc.perform(delete(TASKS_ENDPOINT_BY_ID, taskId))
        .andExpect(status().isNoContent());
  }

  @Test
  void deleteTaskIdNotFound() throws Exception {
    UUID taskId = UUID.randomUUID();

    mockMvc.perform(delete(TASKS_ENDPOINT_BY_ID, taskId))
        .andExpect(status().isNotFound());
  }

  private void checkTasks(ResultActions resultActions) throws Exception {
    resultActions
        .andExpect(jsonPath("$.length()", greaterThan(1)))
        .andExpect(jsonPath("$[*].id").exists())
        .andExpect(jsonPath("$[*].name").exists());
  }

  private void checkTask(ResultActions resultActions) throws Exception {
    resultActions
        .andExpect(jsonPath("$.id").exists())
        .andExpect(jsonPath("$.name").exists());
  }
}