package com.deloitte.assessment.onlinetodolist.rest;

import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createInvalidTaskDto;
import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskDto;
import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskEntity;
import static java.util.Collections.emptyList;
import static java.util.Collections.nCopies;
import static java.util.UUID.randomUUID;
import static org.hamcrest.CoreMatchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import com.deloitte.assessment.onlinetodolist.exception.DuplicatedException;
import com.deloitte.assessment.onlinetodolist.exception.NotFoundException;
import com.deloitte.assessment.onlinetodolist.model.entity.Task;
import com.deloitte.assessment.onlinetodolist.service.TaskService;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.util.List;
import java.util.UUID;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

//@ExtendWith(SpringExtension.class)
@WebMvcTest(controllers = TaskController.class, secure = false)
//@AutoConfigureMockMvc()
//@WithMockUser
class TaskControllerTest {

  private static final String TASK_ENDPOINT = "/tasks";
  private static final String TASK_ENDPOINT_BY_ID = "/tasks/{id}";
  private final ObjectMapper objectMapper = new ObjectMapper();

  @Autowired
  WebApplicationContext webApplicationContext;
  //  @Autowired
  private MockMvc mockMvc;
  @MockBean
  private TaskService taskService;

  @BeforeEach
  void setup() {
    mockMvc = MockMvcBuilders.webAppContextSetup(webApplicationContext).build();

//    mockMvc = MockMvcBuilders
//        .webAppContextSetup(webApplicationContext)
//        .apply(springSecurity())
//        .build();
  }

  @Test
  @DisplayName("Return 201(Created) when successfully adding a new task")
  void addTaskRespondsWith201Code() throws Exception {
    when(taskService.addTask(any())).thenReturn(createTaskEntity("Sample task name"));

    mockMvc.perform(post(TASK_ENDPOINT).contentType(APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createTaskDto())))
        .andExpect(status().isCreated());
  }

  @Test
  @DisplayName("Return 400(Bad request) when adding a task with required fields not present")
  void addInvalidTaskRespondsWith400Code() throws Exception {
    mockMvc.perform(post(TASK_ENDPOINT).contentType(APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createInvalidTaskDto())))
        .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("Return 400(Bad request) when adding a task with an already existing name")
  void addExistingTaskRespondsWith400Code() throws Exception {
    when(taskService.addTask(any())).thenThrow(new DuplicatedException("Task with the same name already exists"));

    mockMvc.perform(post(TASK_ENDPOINT).contentType(APPLICATION_JSON)
        .content(objectMapper.writeValueAsString(createTaskDto())))
        .andExpect(status().isBadRequest());
  }

  @Test
  @DisplayName("Return 200(OK) when successfully getting all tasks")
  void getAllTasks() throws Exception {
    List<Task> tasks = nCopies(2, createTaskEntity("Sample task name"));
    when(taskService.getAllTasks()).thenReturn(tasks);

    mockMvc.perform(get(TASK_ENDPOINT))
        .andExpect(status().isOk())
        .andExpect(jsonPath("$.length()", is(tasks.size())))
        .andExpect(jsonPath("$[*].id").exists())
        .andExpect(jsonPath("$[*].name").exists());
  }

  @Test
  @DisplayName("Return 200(OK) when no tasks are found")
  void getAllTasksNotFound() throws Exception {
    when(taskService.getAllTasks()).thenReturn(emptyList());

    mockMvc.perform(get(TASK_ENDPOINT))
        .andExpect(status().isOk())
        .andExpect(content().string("[]"));
  }

  @Test
  @DisplayName("Return 500(Internal Server error) when getting a task and an unexpected exception happens")
  void getTaskByIdRespondsWith500Code() throws Exception {
    UUID id = randomUUID();
    when(taskService.getAllTasks()).thenThrow(new RuntimeException());

    mockMvc.perform(get(TASK_ENDPOINT, id))
        .andExpect(status().isInternalServerError());
  }

  @Test
  @DisplayName("Return 204(NO CONTENT) when deleting a task")
  void deleteTaskRespondsWith204Code() throws Exception {
    UUID id = randomUUID();

    mockMvc.perform(delete(TASK_ENDPOINT_BY_ID, id))
        .andExpect(status().isNoContent());
    verify(taskService, times(1)).deleteTask(id);
  }

  @Test
  @DisplayName("Return 404(NOT FOUND) when deleting by Id and Id supplied was not found.")
  void deleteTaskRespondsWith404Code() throws Exception {
    UUID id = UUID.randomUUID();
    doThrow(new NotFoundException("Could not find a task with the specified id")).when(taskService).deleteTask(id);

    mockMvc.perform(delete(TASK_ENDPOINT_BY_ID, id))
        .andExpect(status().isNotFound());
  }
}