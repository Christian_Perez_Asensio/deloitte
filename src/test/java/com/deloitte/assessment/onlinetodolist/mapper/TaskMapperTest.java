package com.deloitte.assessment.onlinetodolist.mapper;

import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskDto;
import static com.deloitte.assessment.onlinetodolist.util.TaskBuilderUtils.createTaskEntity;
import static com.deloitte.assessment.onlinetodolist.util.TestUtils.attemptToCreateInstanceOf;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import com.deloitte.assessment.onlinetodolist.model.dto.TaskDto;
import com.deloitte.assessment.onlinetodolist.model.entity.Task;
import java.util.UUID;
import org.junit.jupiter.api.Test;

class TaskMapperTest {

  @Test
  void convertFromDto() {
    TaskDto taskDto = createTaskDto();
    taskDto.setId(UUID.randomUUID());

    Task Task = TaskMapper.convertFromDto(taskDto);

    assertEquals(taskDto.getId(), Task.getId());
    assertEquals(taskDto.getName(), Task.getName());
  }

  @Test
  void convertToDto() {
    Task task = createTaskEntity("Sample task name");
    task.setId(UUID.randomUUID());

    TaskDto TaskDTO = TaskMapper.convertToDto(task);

    assertEquals(task.getId(), TaskDTO.getId());
    assertEquals(task.getName(), TaskDTO.getName());
  }

  @Test
  void unableToCreateInstanceOfTaskMapper() {
    assertThrows(AssertionError.class, () -> attemptToCreateInstanceOf(TaskMapper.class));
  }
}