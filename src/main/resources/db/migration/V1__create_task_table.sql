create table task (
  id            uuid     not null,
  name          varchar  not null,
  primary key (id)
);