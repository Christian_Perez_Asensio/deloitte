alter table if exists task add constraint rule_unique_name unique (name);
alter table if exists task add column username varchar;
alter table if exists task add constraint rule_fk_user foreign key (username) references users(username);

insert into task(id,name,username) values ('523f9d4c-7def-4570-a287-17e160e99069','Sample task name 1','user1');
insert into task(id,name,username) values ('f8f557de-62e5-480d-b9ad-db36c7b37885','Sample task name 2','user1');
insert into task(id,name,username) values ('a54bd101-1082-4ae2-a2b2-fe654dbb9aa4','Sample task name 3','user1');
insert into task(id,name,username) values ('a35a162d-764f-4a28-a533-c3cfa8c53e2d','Sample task name 4','user2');
insert into task(id,name,username) values ('bfa1a3bb-7128-4696-8bf1-0dfd49d3bbf5','Sample task name 5','user2');