package com.deloitte.assessment.onlinetodolist;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class OnlineTodoListApi {

  public static void main(String[] args) {
    SpringApplication.run(OnlineTodoListApi.class);
  }
}