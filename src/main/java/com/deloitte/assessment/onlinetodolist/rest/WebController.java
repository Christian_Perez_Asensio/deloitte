package com.deloitte.assessment.onlinetodolist.rest;

import static com.deloitte.assessment.onlinetodolist.mapper.TaskMapper.convertToDto;
import static java.util.Arrays.asList;

import java.security.Principal;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.http.client.support.BasicAuthorizationInterceptor;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.ModelAndView;

@Slf4j
@Controller
public class WebController {

  @RequestMapping("/login")
  public String login() {
    return "login";
  }

  @RequestMapping(value = "/username", method = RequestMethod.GET)
  @ResponseBody
  public String currentUserName(Principal principal) {
    return principal.getName();
  }

  @RequestMapping(value = "/", method = RequestMethod.GET)
  public ModelAndView home() {
    Object user = getUser();
    ModelAndView modelAndView = new ModelAndView("index");
    modelAndView.addObject("username", getUsername(user));
    RestTemplate restTemplate = new RestTemplate();

    restTemplate.setMessageConverters(asList(new FormHttpMessageConverter(), new StringHttpMessageConverter()));
    restTemplate.getInterceptors().add(new BasicAuthorizationInterceptor(getUsername(user), getPassword(user)));

    String resourceUrl = "http://localhost:8080/tasks";
    ResponseEntity<String> response = restTemplate.getForEntity(resourceUrl, String.class);
    modelAndView.addObject("tasks", convertToDto(response.getBody()));

    return modelAndView;
  }

  private Object getUser() {
    return SecurityContextHolder.getContext().getAuthentication().getPrincipal();
  }

  private String getUsername(Object user) {
    if (user instanceof UserDetails) {
      return ((UserDetails) user).getUsername();
    } else {
      return user.toString();
    }
  }

  private String getPassword(Object user) {
    if (user instanceof UserDetails) {
      return ((UserDetails) user).getPassword();
    } else {
      return user.toString();
    }
  }
}