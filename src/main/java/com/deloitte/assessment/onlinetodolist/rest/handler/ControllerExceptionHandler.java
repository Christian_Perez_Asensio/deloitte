package com.deloitte.assessment.onlinetodolist.rest.handler;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import com.deloitte.assessment.onlinetodolist.exception.DuplicatedException;
import com.deloitte.assessment.onlinetodolist.exception.NotFoundException;
import com.deloitte.assessment.onlinetodolist.exception.model.ApiError;
import com.deloitte.assessment.onlinetodolist.exception.model.ApiErrorResponse;
import java.util.List;
import java.util.stream.Collectors;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@Slf4j
@RestControllerAdvice
public class ControllerExceptionHandler extends ResponseEntityExceptionHandler {

  @Override
  protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException exception,
      HttpHeaders headers, HttpStatus status, WebRequest request) {
    log.error("Invalid request ", exception);

    List<ApiError> errors = exception.getBindingResult().getFieldErrors().stream()
        .map(error -> new ApiError("001", String
            .format("Field {%s} validation failed with the following message : %s", error.getField(),
                error.getDefaultMessage())))
        .collect(Collectors.toList());

    return super.handleExceptionInternal(exception, new ApiErrorResponse(errors), new HttpHeaders(), BAD_REQUEST,
        request);
  }

  @ExceptionHandler(NotFoundException.class)
  @ResponseStatus(NOT_FOUND)
  protected ApiErrorResponse handleNotFoundException(NotFoundException exception) {
    log.error("The resource requested was not found ", exception);
    return new ApiErrorResponse(new ApiError("002", exception.getMessage()));
  }

  @ExceptionHandler(DuplicatedException.class)
  @ResponseStatus(BAD_REQUEST)
  protected ApiErrorResponse handleDuplicateException(DuplicatedException exception) {
    log.error("The resource failed on unique constraint", exception);
    return new ApiErrorResponse(new ApiError("003", exception.getMessage()));
  }

  @ExceptionHandler({Throwable.class})
  @ResponseStatus(INTERNAL_SERVER_ERROR)
  protected ApiErrorResponse handleUnexpectedExceptions(Throwable throwable) {
    log.error("An unexpected error has occurred", throwable);
    return new ApiErrorResponse(new ApiError("004", "An unexpected error has occurred on our side."));
  }
}