package com.deloitte.assessment.onlinetodolist.rest;

import static com.deloitte.assessment.onlinetodolist.mapper.TaskMapper.convertFromDto;
import static com.deloitte.assessment.onlinetodolist.mapper.TaskMapper.convertToDto;
import static java.util.stream.Collectors.toList;
import static org.springframework.http.HttpStatus.CREATED;
import static org.springframework.http.HttpStatus.NO_CONTENT;
import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

import com.deloitte.assessment.onlinetodolist.mapper.TaskMapper;
import com.deloitte.assessment.onlinetodolist.model.dto.TaskDto;
import com.deloitte.assessment.onlinetodolist.model.entity.Task;
import com.deloitte.assessment.onlinetodolist.service.TaskService;
import java.util.List;
import java.util.UUID;
import javax.validation.Valid;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequestMapping("/tasks")
public class TaskController {

  @Autowired
  private TaskService taskService;

  @PostMapping(produces = APPLICATION_JSON_VALUE, consumes = APPLICATION_JSON_VALUE)
  @ResponseStatus(CREATED)
  public TaskDto addTask(@Valid @RequestBody TaskDto taskDto) {
    log.info("Request to add a new task: {}", taskDto);
    Task addedTask = taskService.addTask(convertFromDto(taskDto));
    return convertToDto(addedTask);
  }

  @GetMapping(produces = APPLICATION_JSON_VALUE)
  public List<TaskDto> getAllTasks() {
    log.info("Request to get all tasks");
    return taskService.getAllTasks().stream().map(TaskMapper::convertToDto).collect(toList());
  }

  @DeleteMapping(value = "/{id}", produces = APPLICATION_JSON_VALUE)
  @ResponseStatus(NO_CONTENT)
  public void deleteTask(@PathVariable("id") UUID id) {
    log.info("Deleting task with id: {}", id);
    taskService.deleteTask(id);
  }
}
