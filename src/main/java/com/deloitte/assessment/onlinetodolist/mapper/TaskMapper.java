package com.deloitte.assessment.onlinetodolist.mapper;

import com.deloitte.assessment.onlinetodolist.model.dto.TaskDto;
import com.deloitte.assessment.onlinetodolist.model.entity.Task;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import org.modelmapper.ModelMapper;
import org.modelmapper.TypeToken;

public class TaskMapper {

  private static ModelMapper modelMapper = new ModelMapper();

  private TaskMapper() {
    throw new AssertionError("Suppress default constructor to prevent instantiation");
  }

  public static Task convertFromDto(TaskDto taskDto) {
    return modelMapper.map(taskDto, Task.class);
  }

  public static TaskDto convertToDto(Task task) {
    return modelMapper.map(task, TaskDto.class);
  }

  public static List<TaskDto> convertToDto(String task) {
    Type listType = new TypeToken<ArrayList<TaskDto>>() {}.getType();
    return modelMapper.map(task, listType);
  }
}