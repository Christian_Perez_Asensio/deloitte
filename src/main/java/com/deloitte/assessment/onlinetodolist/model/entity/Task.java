package com.deloitte.assessment.onlinetodolist.model.entity;

import java.util.UUID;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import lombok.Data;
import org.hibernate.annotations.GenericGenerator;

@Data
@Entity
public class Task {

  @Id
  @GeneratedValue(generator = "generator_rule_id")
  @GenericGenerator(name = "generator_rule_id", strategy = "uuid2")
  private UUID id;
  @NotNull
  @Column(unique = true)
  private String name;
  private String username;
}