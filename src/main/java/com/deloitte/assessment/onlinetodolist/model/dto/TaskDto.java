package com.deloitte.assessment.onlinetodolist.model.dto;

import java.util.UUID;
import javax.validation.constraints.NotNull;
import lombok.Data;

@Data
public class TaskDto {

  private UUID id;
  @NotNull
  private String name;
}