package com.deloitte.assessment.onlinetodolist.exception.model;

import lombok.Data;

@Data
public class ApiError {

  private final String code;
  private final String reason;
}