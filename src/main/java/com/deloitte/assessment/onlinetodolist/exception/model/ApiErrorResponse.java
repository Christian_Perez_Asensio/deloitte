package com.deloitte.assessment.onlinetodolist.exception.model;

import static java.util.Collections.singletonList;

import java.util.List;
import lombok.Data;

@Data
public class ApiErrorResponse {

  private List<ApiError> errors;

  public ApiErrorResponse(List<ApiError> errors) {
    this.errors = errors;
  }

  public ApiErrorResponse(ApiError error) {
    errors = singletonList(error);
  }
}