package com.deloitte.assessment.onlinetodolist.service;

import static org.springframework.util.Assert.notNull;

import com.deloitte.assessment.onlinetodolist.exception.DuplicatedException;
import com.deloitte.assessment.onlinetodolist.exception.NotFoundException;
import com.deloitte.assessment.onlinetodolist.model.entity.Task;
import com.deloitte.assessment.onlinetodolist.repository.TaskRepository;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class TaskService {

  @Autowired
  private TaskRepository taskRepository;

  public Task addTask(Task task) {
    notNull(task, "task must not be null");
    validate(task.getName());
    return taskRepository.save(task);
  }

  public List<Task> getAllTasks() {
    return taskRepository.findAll();
  }

  public void deleteTask(UUID id) {
    Optional<Task> optionalTask = taskRepository.findById(id);
    if (optionalTask.isPresent()) {
      taskRepository.delete(optionalTask.get());
    } else {
      throw new NotFoundException("Could not find a task with the specified id");
    }
  }

  /**
   * Added feature, just to add some validation and exception handling. Multiple tasks should not have same name.
   *
   * @param name - name of the task
   */
  private void validate(String name) {
    if (taskRepository.findByName(name).isPresent()) {
      throw new DuplicatedException(String.format("A task with the same name <%s> already exists", name));
    }
  }
}
